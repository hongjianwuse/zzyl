package com.zzyl.controller.customer;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(tags = "客户管理")
@RestController
@RequestMapping("/customer/user")
public class CustomerUserController {

    //TODO @ApiOperation("C端用户登录--微信登录")

    // ================================================= 家属端数据报表展示

    //TODO @ApiOperation("查询指定设备的物模型运行状态")
    //TODO @ApiOperation("按日查询设备数据")
    //TODO @ApiOperation("按周查询设备数据")
}
