package com.zzyl.controller.customer;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer/reservation")
@Api(tags = "客户预约管理")
public class CustomerReservationController {

    //TODO @ApiOperation("查询取消预约数量")
    //TODO @ApiOperation("查询每个时间段剩余预约次数")
    //TODO @ApiOperation("新增预约")
    //TODO @ApiOperation("分页查询预约")
    //TODO @ApiOperation("取消预约")
    //TODO @ApiOperation("查询所有预约")
}
