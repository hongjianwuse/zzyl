package com.zzyl.controller.customer;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.MemberElderDto;
import com.zzyl.service.MemberElderService;
import com.zzyl.vo.MemberElderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer/memberElder")
@Api(tags = "客户老人关联管理")
public class MemberElderController {

    @Autowired
    private MemberElderService memberElderService;

    @GetMapping("/my")
    @ApiOperation(value = "我的家人列表")
    public ResponseResult<List<MemberElderVo>> my() {
        List<MemberElderVo> memberElders = memberElderService.my();
        return ResponseResult.success(memberElders);
    }

    @GetMapping("/list-by-page")
    @ApiOperation(value = "分页查询客户老人关联记录")
    public ResponseResult<PageBean<MemberElderVo>> listByPage(Long memberId, Long elderId,
                                                              Integer pageNum, Integer pageSize) {
        PageBean<MemberElderVo> pageInfo = memberElderService.listByPage(memberId, elderId, pageNum, pageSize);
        return ResponseResult.success(pageInfo);
    }

    @PostMapping("/add")
    @ApiOperation(value = "新增客户老人关联记录", notes = "绑定家人")
    public ResponseResult<Void> add(@RequestBody MemberElderDto memberElderDto) {
        memberElderService.add(memberElderDto);
        return ResponseResult.success();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(value = "根据id删除客户老人关联记录")
    public ResponseResult<Void> deleteById(@RequestParam Long id) {
        memberElderService.deleteById(id);
        return ResponseResult.success();
    }
}


