package com.zzyl.controller.manager;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.service.OrderService;
import com.zzyl.vo.OrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(tags = "订单管理相关接口")
@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @ApiOperation("取消")
    @PostMapping("/{orderId}/cancel")
    public ResponseResult<OrderVo> cancelOrder(@PathVariable("orderId") Long orderId,
                                               @RequestParam("reason") String reason) {
        OrderVo orderVo = orderService.cancelOrder(orderId, reason, 2);
        return ResponseResult.success(orderVo);
    }

    @ApiOperation("根据id查询")
    @GetMapping
    public ResponseResult<OrderVo> getOrderById(@RequestParam("orderId") Long orderId) {
        OrderVo orderList = orderService.getOrderById(orderId);
        return ResponseResult.success(orderList);
    }

    @ApiOperation("分页")
    @GetMapping("/search")
    public ResponseResult<PageBean<OrderVo>> searchOrders(@RequestParam(value = "status", required = false) Integer status,
                                                          @RequestParam(value = "orderNo", required = false) String orderNo,
                                                          @RequestParam(value = "elderlyName", required = false) String elderlyName,
                                                          @RequestParam(value = "creator", required = false) String creator,
                                                          @RequestParam(required = false) Long startTime,
                                                          @RequestParam(required = false) Long endTime,
                                                          @RequestParam("pageNum") Integer pageNum,
                                                          @RequestParam("pageSize") Integer pageSize) {
        PageBean<OrderVo> pageBean = orderService.searchOrders(status, orderNo, elderlyName, creator, ObjectUtil.isEmpty(startTime) ? null : LocalDateTimeUtil.of(startTime), ObjectUtil.isEmpty(endTime) ? null : LocalDateTimeUtil.of(endTime), pageNum, pageSize);
        return ResponseResult.success(pageBean);
    }

    @ApiOperation("执行")
    @PostMapping("/{orderId}/do")
    public ResponseResult<OrderVo> doOrder(@PathVariable("orderId") Long orderId) {
        OrderVo orderVo = orderService.doOrder(orderId);
        return ResponseResult.success(orderVo);
    }
}

