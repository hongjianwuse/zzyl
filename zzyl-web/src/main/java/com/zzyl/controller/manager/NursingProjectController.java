package com.zzyl.controller.manager;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/nursing_project")
@Api(tags = "护理项目管理口")
public class NursingProjectController {

    //TODO @ApiOperation("分页条件查询护理项目")
    //TODO @ApiOperation("新增护理项目")
    //TODO @ApiOperation("根据id查询护理项目")
    //TODO @ApiOperation("修改护理项目")
    //TODO @ApiOperation("查询所有护理项目")
}
