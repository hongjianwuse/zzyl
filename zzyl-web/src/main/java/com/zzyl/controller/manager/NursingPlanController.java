package com.zzyl.controller.manager;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/nursing")
@Api(tags = "护理计划管理")
public class NursingPlanController {

    //TODO @ApiOperation("添加护理计划")
    //TODO @ApiOperation("修改护理计划")
    //TODO @ApiOperation("删除护理计划")
    //TODO @ApiOperation("根据ID查询护理计划")
    //TODO @ApiOperation("查询所有护理计划")
    //TODO @ApiOperation("根据名称和状态分页查询")
    //TODO @ApiOperation("启用/禁用护理计划")
}
