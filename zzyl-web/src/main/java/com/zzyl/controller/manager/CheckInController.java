package com.zzyl.controller.manager;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.service.CheckInService;
import com.zzyl.vo.CheckInDetailVo;
import com.zzyl.vo.CheckInPageQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/check-in")
@Api(tags = "入住管理相关接口")
public class CheckInController {

    @Autowired
    private CheckInService checkInService;

    @GetMapping("/pageQuery")
    @ApiOperation(value = "分页查询入住列表")
    public ResponseResult<PageBean<CheckInPageQueryVo>> pageQuery(@RequestParam(value = "elderName", required = false) String elderName,
                                                                  @RequestParam(value = "idCardNo", required = false) String idCardNo,
                                                                  @RequestParam("pageNum") Integer pageNum,
                                                                  @RequestParam("pageSize") Integer pageSize) {
        return ResponseResult.success(checkInService.pageQuery(elderName, idCardNo, pageNum, pageSize));
    }

    @GetMapping("/detail/{id}")
    @ApiOperation(value = "查询入住详情")
    public ResponseResult<CheckInDetailVo> detail(@PathVariable("id") Long id) {
        return ResponseResult.success(checkInService.detail(id));
    }
}

