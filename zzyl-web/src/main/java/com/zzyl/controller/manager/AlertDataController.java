package com.zzyl.controller.manager;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/alert-data")
@Api(tags = "报警数据相关接口")
public class AlertDataController {

    //TODO @ApiOperation(value = "分页查询报警数据")
    //TODO @ApiOperation("处理设备报警数据")
}
