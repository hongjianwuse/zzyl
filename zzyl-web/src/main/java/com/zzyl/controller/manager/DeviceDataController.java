package com.zzyl.controller.manager;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "Iot设备数据管理")
@RequestMapping("/device-data")
public class DeviceDataController {

    //TODO @ApiOperation("分页查询设备中的数据")
}
