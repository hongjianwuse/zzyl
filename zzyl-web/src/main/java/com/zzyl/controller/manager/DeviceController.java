package com.zzyl.controller.manager;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "Iot设备管理")
@RequestMapping("/iot")
public class DeviceController {

    //TODO @ApiOperation("产品列表数据同步")
    //TODO @ApiOperation("查询所有产品列表")
    //TODO @ApiOperation("注册设备")
    //TODO @ApiOperation("分页查询设备")
    //TODO @ApiOperation("查询设备详情")

    //============================================================== 课外作业

    //TODO @ApiOperation(value = "查询指定设备的物模型运行状态")
    //TODO @ApiOperation(value = "查看指定产品的已发布物模型中的功能定义详情")
    //TODO @ApiOperation(value = "修改设备备注名称")
    //TODO @ApiOperation(value = "删除设备")
}
