package com.zzyl.controller.manager;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/nursingLevel")
@Api(tags = "护理等级管理")
public class NursingLevelController {

    //TODO @ApiOperation("查询所有护理等级信息")
    //TODO @ApiOperation("插入护理等级信息")
    //TODO @ApiOperation("更新护理等级信息")
    //TODO @ApiOperation("删除护理等级信息")
    //TODO @ApiOperation("根据ID查询护理等级信息")
    //TODO @ApiOperation("分页查询护理等级信息")
    //TODO @ApiOperation("启用/禁用护理等级")
}