package com.zzyl.controller.manager;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.service.ReservationService;
import com.zzyl.vo.ReservationVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/reservation")
@Api(tags = "预约管理相关接口")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @GetMapping("/page")
    @ApiOperation("分页查询预约")
    public ResponseResult<PageBean<ReservationVo>> findByPage(@RequestParam(defaultValue = "1") int pageNum,
                                                              @RequestParam(defaultValue = "10") int pageSize,
                                                              @RequestParam(required = false) String name,
                                                              @RequestParam(required = false) String phone,
                                                              @RequestParam(required = false) Integer status,
                                                              @RequestParam(required = false) Integer type,
                                                              @RequestParam(required = false) Long startTime,
                                                              @RequestParam(required = false) Long endTime) {
        PageBean<ReservationVo> byPage = reservationService.findByPage(pageNum, pageSize, name, phone, status, type,
                ObjectUtil.isEmpty(startTime) ? null : LocalDateTimeUtil.of(startTime),
                ObjectUtil.isEmpty(endTime) ? null : LocalDateTimeUtil.of(endTime));
        return ResponseResult.success(byPage);
    }


}
