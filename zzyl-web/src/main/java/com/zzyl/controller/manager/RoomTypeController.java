package com.zzyl.controller.manager;

import com.zzyl.base.ResponseResult;
import com.zzyl.dto.RoomTypeDto;
import com.zzyl.service.RoomTypeService;
import com.zzyl.vo.RoomTypeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/roomTypes")
@Api(tags = "房型管理")
public class RoomTypeController {

    @Autowired
    private RoomTypeService roomTypeService;

    @PostMapping
    @ApiOperation("添加房型")
    public ResponseResult<Void> addRoomType(@RequestBody RoomTypeDto roomTypeDTO) {
        roomTypeService.addRoomType(roomTypeDTO);
        return ResponseResult.success();
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除房型")
    public ResponseResult<Void> removeRoomType(@PathVariable Long id) {
        roomTypeService.removeRoomType(id);
        return ResponseResult.success();
    }

    @PutMapping("/{id}")
    @ApiOperation("修改房型")
    public ResponseResult<Void> modifyRoomType(@PathVariable Long id,
                                               @RequestBody RoomTypeDto roomTypeDTO) {
        roomTypeService.modifyRoomType(id, roomTypeDTO);
        return ResponseResult.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("根据ID查询房型")
    public ResponseResult<RoomTypeVo> findRoomTypeById(@PathVariable Long id) {
        RoomTypeVo roomTypeVO = roomTypeService.findRoomTypeById(id);
        return ResponseResult.success(roomTypeVO);
    }

    @GetMapping
    @ApiOperation("查询所有房型")
    public ResponseResult<List<RoomTypeVo>> findRoomTypeList() {
        List<RoomTypeVo> roomTypeVoList = roomTypeService.findRoomTypeList();
        return ResponseResult.success(roomTypeVoList);
    }

    @GetMapping("/status/{status}")
    @ApiOperation("根据状态查询房型")
    public ResponseResult<List<RoomTypeVo>> findRoomTypeListByStatus(@PathVariable Integer status) {
        List<RoomTypeVo> roomTypeVoList = roomTypeService.findRoomTypeListByStatus(status);
        return ResponseResult.success(roomTypeVoList);
    }

    @GetMapping("/typeName/{typeName}")
    @ApiOperation("根据类型名查询房型")
    public ResponseResult<List<RoomTypeVo>> findRoomTypeListByTypeName(@PathVariable String typeName) {
        List<RoomTypeVo> roomTypeVoList = roomTypeService.findRoomTypeListByTypeName(typeName);
        return ResponseResult.success(roomTypeVoList);
    }

    @PutMapping("/{id}/status/{status}")
    @ApiOperation("启用、禁用房型")
    public ResponseResult<Void> enableOrDisable(@PathVariable Long id,
                                                @PathVariable Integer status) {
        roomTypeService.enableOrDisable(id, status);
        return ResponseResult.success();
    }
}
