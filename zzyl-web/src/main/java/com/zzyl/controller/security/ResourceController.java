package com.zzyl.controller.security;

import com.zzyl.base.ResponseResult;
import com.zzyl.service.ResourceService;
import com.zzyl.vo.MenuVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/resource")
@Api(tags = "资源管理")
public class ResourceController {

    @Autowired
    private ResourceService resourceService;

    //TODO @ApiOperation(value = "资源列表")
    //TODO @ApiOperation(value = "资源树形")
    //TODO @ApiOperation(value = "资源添加")

    ///////////////////////////////////////// 作业

    @GetMapping("/menus")
    @ApiOperation("我的菜单")
    public ResponseResult<List<MenuVo>> menus() {
        Long userId = 1671403256519078138L; //假设用户id
        List<MenuVo> menus = resourceService.menus(userId);
        return ResponseResult.success(menus);
    }

    //TODO @ApiOperation(value = "资源修改")
    //TODO @ApiOperation(value = "启用禁用")
    //TODO @ApiOperation(value = "删除菜单")
}
