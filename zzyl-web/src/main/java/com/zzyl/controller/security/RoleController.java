package com.zzyl.controller.security;

import com.zzyl.base.ResponseResult;
import com.zzyl.service.RoleService;
import com.zzyl.vo.RoleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/role")
@Api(tags = "角色管理")
public class RoleController {

    @Autowired
    private RoleService roleService;

    //TODO @ApiOperation(value = "角色分页")
    //TODO @ApiOperation(value = "角色添加")
    //TODO @ApiOperation(value = "根据角色查询选中的资源数据")
    //TODO @ApiOperation(value = "角色修改")
    //TODO @ApiOperation(value = "删除角色")

    /////////////////////////////////////////////////////////////////////////////

    @PostMapping("init-roles")
    @ApiOperation(value = "角色下拉框", notes = "查询所有角色")
    ResponseResult<List<RoleVo>> list() {
        List<RoleVo> roleVoResult = roleService.list();
        return ResponseResult.success(roleVoResult);
    }

}
