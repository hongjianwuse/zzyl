package com.zzyl.controller.security;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(tags = "用户管理")
@RestController
@RequestMapping("/user")
public class UserController {

    //TODO @ApiOperation(value = "用户分页")

    /////////////////////////////////////////////////////////////////（可选）

    //TODO @ApiOperation(value = "用户添加")
    //TODO @ApiOperation(value = "用户修改")
    //TODO @ApiOperation(value = "删除用户")
    //TODO @ApiOperation(value = "启用或禁用用户")
    //TODO @ApiOperation(value = "用户列表（部门管理中需要）")
    //TODO @ApiOperation(value = "密码重置")

}
