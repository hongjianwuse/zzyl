package com.zzyl.controller.security;

import com.google.common.collect.Maps;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.LoginDto;
import com.zzyl.utils.JwtUtil;
import com.zzyl.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/security")
@Api(tags = "后台登录")
public class LoginController {

    @ApiOperation("后台用户登录")
    @PostMapping("/login")
    public ResponseResult<UserVo> login(@RequestBody LoginDto loginDto) {

        //假数据
        UserVo userVo = new UserVo();
        Map<String, Object> map = Maps.newHashMap();
        map.put("username", loginDto.getUsername());

        userVo.setUserToken(JwtUtil.createJWT("itheima", 600000, map));
        return ResponseResult.success(userVo);
    }
}
