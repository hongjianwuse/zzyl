package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.constant.TradingConstant;
import com.zzyl.entity.RefundRecord;
import com.zzyl.enums.RefundStatusEnum;
import com.zzyl.mapper.RefundRecordMapper;
import com.zzyl.service.RefundRecordService;
import com.zzyl.utils.WeChatPayUtil;
import com.zzyl.vo.RefundRecordVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 退款记录服务实现类
 */
@Service
public class RefundRecordServiceImpl implements RefundRecordService {

    @Resource
    private RefundRecordMapper refundRecordMapper;
    @Resource
    private WeChatPayUtil weChatPayUtil;

    /**
     * 根据退款编号查询退款记录
     *
     * @param refundNo 退款编号
     * @return 退款记录
     */
    @Override
    public RefundRecordVo findByRefundNo(Long refundNo) {
        return refundRecordMapper.selectByRefundNo(refundNo);
    }

    /**
     * 根据交易订单编号查询退款记录列表
     *
     * @param tradingOrderNo 交易订单编号
     * @return 退款记录列表
     */
    @Override
    public List<RefundRecord> findListByTradingOrderNo(Long tradingOrderNo) {
        return refundRecordMapper.selectListByTradingOrderNo(tradingOrderNo);
    }

    /**
     * 根据商品订单编号查询退款记录列表
     *
     * @param productOrderNo 商品订单编号
     * @return 退款记录列表
     */
    @Override
    public List<RefundRecord> findListByProductOrderNo(Long productOrderNo) {
        return refundRecordMapper.selectListByProductOrderNo(productOrderNo);
    }

    /**
     * 根据退款状态查询退款记录列表
     *
     * @param refundStatus 退款状态
     * @param count        查询数量
     * @return 退款记录列表
     */
    @Override
    public List<RefundRecord> findListByRefundStatus(RefundStatusEnum refundStatus, Integer count) {
        count = NumberUtil.max(count, 10);
        return refundRecordMapper.selectListByRefundStatus(refundStatus.getCode(), count);
    }

    /**
     * 保存或更新退款记录
     *
     * @param refundRecord 退款记录
     */
    @Override
    public void saveOrUpdate(RefundRecord refundRecord) {
        if (refundRecord.getId() == null) {
            refundRecordMapper.insert(refundRecord);
        } else {
            refundRecordMapper.updateByPrimaryKey(refundRecord);
        }
    }

    @Override
    public RefundRecord findRefundRecordByProductOrderNoAndSending(Long productOrderNo) {
        return null;
    }

    @Override
    public PageBean<RefundRecordVo> queryRefundRecord(RefundRecordVo refundRecordVo) {
        try {
            if (ObjectUtil.isNotEmpty(refundRecordVo.getRefundNoStr())) {
                Long aLong = Long.valueOf(refundRecordVo.getRefundNoStr());
                refundRecordVo.setRefundNo(aLong);
            }
        } catch (Exception e) {
            return PageBean.of(new Page<>(), RefundRecordVo.class);
        }
        PageHelper.startPage(refundRecordVo.getPageNum(), refundRecordVo.getPageSize());
        Page<List<RefundRecordVo>> lists = refundRecordMapper.queryRefundRecord(refundRecordVo);
        return PageBean.of(lists, RefundRecordVo.class);
    }

    /**
     * 查询第三方退款信息
     *
     * @param refundRecordVo 退款记录
     * @return 退款记录
     */
    @Override
    public RefundRecordVo queryRefundTrading(RefundRecordVo refundRecordVo) {
        //1、退款前置处理：检测退款单参数
        RefundRecordVo refundRecordHandler = findByRefundNo(refundRecordVo.getRefundNo());
        String refundJsonStr = weChatPayUtil.queryRefund(String.valueOf(refundRecordVo.getRefundNo()));
        JSONObject refundJsonObject = JSONUtil.parseObj(refundJsonStr);

        /*
        退款状态枚举值：
        SUCCESS：退款成功
        CLOSED：退款关闭
        PROCESSING：退款处理中
        ABNORMAL：退款异常
        */
        if (TradingConstant.WECHAT_REFUND_SUCCESS.equals(refundJsonObject.getStr("status"))) {
            refundRecordHandler.setRefundStatus(TradingConstant.REFUND_STATUS_SUCCESS_2);
            saveOrUpdate(BeanUtil.toBean(refundRecordHandler, RefundRecord.class));
        }

        return BeanUtil.toBean(refundRecordHandler, RefundRecordVo.class);
    }
}


