package com.zzyl.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 阿里云Iot配置类
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "zzyl.aliyun.iot")
public class AliyunIotProperties {

    private String accessKeyId;//访问Key
    private String accessKeySecret;//访问秘钥
    private String regionId;//区域id
    private String iotInstanceId;//实例id
    private String host;//域名
    private String consumerGroupId;//消费组
}