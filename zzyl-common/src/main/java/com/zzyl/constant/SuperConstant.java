package com.zzyl.constant;


/**
 * 静态变量
 */
public class SuperConstant {

    /**
     * 常量：是
     */
    public static final String DATA_STATE_0 = "0";

    /**
     * 常量：否
     */
    public static final String DATA_STATE_1 = "1";

    /**
     * 树形根节点父Id
     */
    public static final String ROOT_RESOURCE_PARENT_ID = "100001000000000";

    /**
     * 树形根节点父Id
     */
    public static final String ROOT_DEPT_PARENT_ID = "100000000000000";

    /**
     * 常量：目录
     */
    public static final String CATALOGUE = "c";

    /**
     * 常量：菜单
     */
    public static final String MENU = "m";

    /**
     * 常量：按钮
     */
    public static final String BUTTON = "r";

    /**
     * 常量：平台
     */
    public static final String SYSTEM = "s";

    /**
     * 前端显示布局
     */
    public static final String COMPONENT_LAYOUT = "Layout";

    /**
     * 常量：是
     */
    public static final String DEFAULT_0 = "0";
}
