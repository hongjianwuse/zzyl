package com.zzyl.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.CreateBucketRequest;
import com.aliyun.oss.model.SetBucketLoggingRequest;
import com.zzyl.properties.AliyunOssProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 阿里云Oss配置类
 */
@Slf4j
@Configuration
public class AliyunOssConfig {

    @Autowired
    private AliyunOssProperties aliyunOssProperties;

    @Bean
    public OSS ossClient() {
        OSS ossClient = new OSSClientBuilder().build(aliyunOssProperties.getEndpoint(),
                aliyunOssProperties.getAccessKeyId(),
                aliyunOssProperties.getAccessKeySecret());
        //判断容器是否存在,不存在就创建
        if (!ossClient.doesBucketExist(aliyunOssProperties.getBucketName())) {
            ossClient.createBucket(aliyunOssProperties.getBucketName());
            CreateBucketRequest createBucketRequest = new CreateBucketRequest(aliyunOssProperties.getBucketName());
            //设置问公共可读
            createBucketRequest.setCannedACL(CannedAccessControlList.PublicRead);
            ossClient.createBucket(createBucketRequest);
        }

        //添加客户端访问日志
        SetBucketLoggingRequest request = new SetBucketLoggingRequest(aliyunOssProperties.getBucketName());
        // 设置存放日志文件的存储空间。
        request.setTargetBucket(aliyunOssProperties.getBucketName());
        // 设置日志文件存放的目录。
        request.setTargetPrefix(aliyunOssProperties.getBucketName());
        ossClient.setBucketLogging(request);

        return ossClient;
    }


}
