package com.zzyl.base;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ApiModel("Vo基础类")
public class BaseVo implements Serializable {

    private Long id; // 主键
    protected LocalDateTime createTime; // 创建时间
    protected String createDay; // 创建时间
    protected LocalDateTime updateTime; // 修改时间
    private Long createBy; // 创建者:username
    private Long updateBy; // 更新者:username
    protected String dataState; // 是否有效
    private String remark; // 备注
    private Integer createType; // 创建人类型 1 前台 2后台
    private String creator; // 创建人名称
    private String adminCreator; // 后台管理端创建人名称
    private String updater; // 更新人名称
}
