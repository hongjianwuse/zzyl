package com.zzyl.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@SuperBuilder
@ApiModel(description = "实体基础类")
public class BaseEntity implements Serializable {

    public Long id; // 主键
    public LocalDateTime createTime; // 创建时间
    public LocalDateTime updateTime; // 更新时间
    private Long createBy; // 创建人
    private Long updateBy; // 更新人
    private String remark; // 备注
    private String creator; // 创建人
    private String updater; // 更新人

    @JsonIgnore
    private Map<String, Object> params = new HashMap<>();
}
