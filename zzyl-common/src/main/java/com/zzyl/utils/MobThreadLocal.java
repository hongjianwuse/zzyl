package com.zzyl.utils;

import lombok.extern.slf4j.Slf4j;

/**
 * Mobile本地线程维护工具
 */
@Slf4j
public class MobThreadLocal {

    /**
     * 移动端操作的本地线程
     */
    private static final ThreadLocal<Long> threadLocal = new ThreadLocal<>();


    public static void set(Long userId) {
        threadLocal.set(userId);
    }

    public static Long getUserId() {
        return threadLocal.get();
    }

    public static void remove() {
        threadLocal.remove();
    }
}
