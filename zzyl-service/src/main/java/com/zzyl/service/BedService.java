package com.zzyl.service;

import com.zzyl.vo.BedVo;

import java.util.List;

public interface BedService {

    /**
     * 通过房间ID检索床位
     *
     * @param roomId 房间ID
     * @return 床位视图对象列表
     */
    List<BedVo> getBedsByRoomId(Long roomId);
}
