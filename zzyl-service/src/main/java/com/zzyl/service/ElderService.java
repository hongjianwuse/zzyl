package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.dto.ElderDto;
import com.zzyl.dto.NursingElderDto;
import com.zzyl.entity.Elder;
import com.zzyl.vo.ElderCheckInInfoVo;
import com.zzyl.vo.ElderPageQueryVo;
import com.zzyl.vo.retreat.ElderVo;

import java.util.List;

public interface ElderService {
    /**
     * 根据id删除老人信息
     *
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增或更新老人信息
     *
     * @param elder 新增或更新老人模型
     * @return 老人信息
     */
    Elder insertOrUpdate(Elder elder);

    /**
     * 选择性插入老人信息
     *
     * @param record
     * @return
     */
    int insertSelective(ElderDto record);

    /**
     * 根据id选择老人信息
     *
     * @param id
     * @return
     */
    ElderVo selectByPrimaryKey(Long id);

    /**
     * 选择性更新老人信息
     *
     * @param record
     * @param b
     * @return
     */
    Elder updateByPrimaryKeySelective(ElderDto record, boolean b);

    /**
     * 更新老人信息
     *
     * @param record
     * @return
     */
    int updateByPrimaryKey(ElderDto record);

    /**
     * 根据身份证号和姓名查询老人信息
     *
     * @param idCard
     * @param name
     * @return
     */
    ElderVo selectByIdCardAndName(String idCard, String name);

    /**
     * 查询所有老人
     *
     * @return
     */
    List<ElderVo> selectList();

    /**
     * 根据id集合查询老人列表
     *
     * @param ids
     * @return
     */
    List<Elder> selectByIds(List<Long> ids);

    /**
     * 设置护理员
     *
     * @param nursingElders
     */
    void setNursing(List<NursingElderDto> nursingElders);

    /**
     * 根据身份证号和名字查询老人
     *
     * @param idCard
     * @param name
     * @return
     */
    ElderVo selectByIdCardAndStatus(String idCard, String name);

    /**
     * 根据身份证号和状态查询老人
     *
     * @param idCard 身份证号
     * @param status 状态{@link com.zzyl.enums.ElderStatusEnum}
     * @return 老人信息
     */
    ElderVo selectByIdCardAndStatus(String idCard, Integer status);


    /**
     * 清除老人床位编号
     *
     * @param elderId
     */
    void clearBedNum(Long elderId);

    /**
     * 老人信息分页查询
     *
     * @param name     老人姓名，模糊查询
     * @param idCardNo 身份证号，精确查询
     * @param status   状态，0：入住中，1：已退住
     * @param pageNum  页码
     * @param pageSize 页面大小
     * @return 分页结果
     */
    PageBean<ElderPageQueryVo> pageQuery(String name, String idCardNo, String status, Integer pageNum, Integer pageSize);

    /**
     * 老人入住信息查询
     *
     * @param id 老人id
     * @return 老人入住信息
     */
    ElderCheckInInfoVo checkInInfo(Long id);
}

