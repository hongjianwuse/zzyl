package com.zzyl.service;

public interface WechatService {

    /**
     * 获取openid
     *
     * @param code 登录凭证
     * @return
     */
    String getOpenid(String code);

    /**
     * 获取手机号
     *
     * @param code 手机号凭证
     * @return
     */
    String getPhone(String code);
}