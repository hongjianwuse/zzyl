package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.json.JSONUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.entity.CheckIn;
import com.zzyl.entity.CheckInConfig;
import com.zzyl.entity.Contract;
import com.zzyl.enums.CheckInStatusEnum;
import com.zzyl.mapper.CheckInConfigMapper;
import com.zzyl.mapper.CheckInMapper;
import com.zzyl.mapper.ContractMapper;
import com.zzyl.service.CheckInService;
import com.zzyl.service.ElderService;
import com.zzyl.vo.*;
import com.zzyl.vo.retreat.ElderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CheckInServiceImpl implements CheckInService {
    @Autowired
    private ElderService elderService;
    @Autowired
    private CheckInMapper checkInMapper;
    @Autowired
    private CheckInConfigMapper checkInConfigMapper;
    @Autowired
    private ContractMapper contractMapper;


    /**
     * 分页查询
     *
     * @param elderName 老人姓名，模糊查询
     * @param idCardNo  身份证号，精确查询
     * @param pageNum   页码
     * @param pageSize  页面大小
     * @return 分页结果
     */
    @Override
    public PageBean<CheckInPageQueryVo> pageQuery(String elderName, String idCardNo, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);

        //老人姓名，模糊查询，这里进行预处理
        elderName = CharSequenceUtil.isBlank(elderName) ? null : "%" + elderName + "%";

        //这里只查询入住状态的
        Page<CheckInPageQueryVo> pageResult = checkInMapper.selectByPage(elderName, idCardNo, CheckInStatusEnum.PROGRESSING.getOrdinal());
        return PageBean.of(pageResult, CheckInPageQueryVo.class);
    }

    /**
     * 入住详情
     *
     * @param id 入住id
     * @return 入住详情
     */
    @Override
    public CheckInDetailVo detail(Long id) {
        //入住信息
        CheckIn checkIn = checkInMapper.selectById(id);

        //老人信息
        ElderVo elderVo = elderService.selectByPrimaryKey(checkIn.getElderId());
        CheckInElderVo checkInElderVo = BeanUtil.toBean(elderVo, CheckInElderVo.class);
        checkInElderVo.setOneInchPhoto(elderVo.getImage());

        //入住配置信息
        CheckInConfig checkInConfig = checkInConfigMapper.findCurrentConfigByElderId(checkIn.getElderId());

        //合同信息
        Contract contract = contractMapper.selectByContractNo(checkIn.getRemark());

        //封装响应信息
        CheckInDetailVo checkInDetailVo = new CheckInDetailVo();
        checkInDetailVo.setCheckInElderVo(checkInElderVo);
        checkInDetailVo.setElderFamilyVoList(JSONUtil.toList(checkIn.getOtherApplyInfo(), ElderFamilyVo.class));
        checkInDetailVo.setCheckInConfigVo(BeanUtil.toBean(checkInConfig, CheckInConfigVo.class));
        checkInDetailVo.setCheckInContractVo(BeanUtil.toBean(contract, CheckInContractVo.class));
        return checkInDetailVo;
    }
}
