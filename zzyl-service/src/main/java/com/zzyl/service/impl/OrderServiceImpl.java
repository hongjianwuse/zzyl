package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.dto.BillDto;
import com.zzyl.dto.OrderDto;
import com.zzyl.entity.*;
import com.zzyl.enums.BasicEnum;
import com.zzyl.enums.BillStatus;
import com.zzyl.enums.ElderStatusEnum;
import com.zzyl.enums.OrderStatus;
import com.zzyl.exception.BaseException;
import com.zzyl.mapper.BillMapper;
import com.zzyl.mapper.NursingTaskMapper;
import com.zzyl.mapper.OrderMapper;
import com.zzyl.service.*;
import com.zzyl.utils.CodeUtil;
import com.zzyl.utils.MobThreadLocal;
import com.zzyl.vo.OrderVo;
import com.zzyl.vo.TradingVo;
import com.zzyl.vo.retreat.ElderVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;
    @Resource
    private MemberService memberService;
    @Resource
    private BillService billService;
    @Resource
    private BillMapper billMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private NursingTaskService nursingTaskService;
    @Resource
    private ElderService elderService;
    @Resource
    private TradingService tradingService;

    /**
     * 创建订单
     *
     * @param orderDto 订单dto
     * @return 订单vo
     */
    @Transactional
    @Override
    public OrderVo createOrder(OrderDto orderDto) {
        ElderVo elderVo = elderService.selectByPrimaryKey(orderDto.getElderId());
        if (elderVo.getStatus() == ElderStatusEnum.RETREAT.getOrdinal()) {
            throw new BaseException(BasicEnum.CANNOT_RESERVATION_DUE_ELDER_ALREADY_RETREATED);
        }

        Order order = BeanUtil.toBean(orderDto, Order.class);
        order.setStatus(OrderStatus.PENDING_PAY.getCode());
        String dd = CodeUtil.generateCode("DD", stringRedisTemplate, 5);
        order.setOrderNo(dd);
        Long userId = MobThreadLocal.getUserId();
        order.setPaymentStatus(1);
        order.setMemberId(userId);
        orderMapper.insert(order);

        Trading trading = new Trading();
        Member member = memberService.getById(userId);
        trading.setOpenId(member.getOpenId());
        trading.setMemo("服务下单");
        trading.setTradingAmount(order.getAmount());
        trading.setProductOrderNo(order.getId());
        trading.setTradingType("2");
        trading.setTradingOrderNo(IdUtil.getSnowflakeNextId());
        TradingVo tradingVo1 = tradingService.createTrading(trading);

        order.setTradingOrderNo(tradingVo1.getTradingOrderNo());
        orderMapper.updateByPrimaryKey(order);

        BillDto billDto = new BillDto();
        billDto.setBillAmount(order.getAmount());
        billDto.setPayableAmount(order.getAmount());
        billDto.setTradingOrderNo(tradingVo1.getTradingOrderNo());
        billDto.setElderId(order.getElderId());
        billService.createProjectBill(billDto);

        OrderVo orderVo = BeanUtil.toBean(order, OrderVo.class);
        orderVo.setTradingVo(tradingVo1);
        return orderVo;
    }

    /**
     * 取消订单
     *
     * @param orderId    订单id
     * @param reason     取消原因
     * @param createType
     * @return 订单vo
     */
    @Override
    public OrderVo cancelOrder(Long orderId, String reason, Integer createType) {

        Order order1 = orderMapper.selectByPrimaryKey(orderId);
        if (order1.getStatus().equals(OrderStatus.CLOSE.getCode())) {
            throw new BaseException(BasicEnum.ORDER_CLOSED);
        }

        Order order = new Order();
        order.setId(orderId);
        order.setStatus(OrderStatus.CLOSE.getCode());
        order.setReason(reason);
        order.setPaymentStatus(3);
        order.setCreateType(createType);
        orderMapper.updateByPrimaryKeySelective(order);

        BillDto billDto = new BillDto();
        billDto.setTradingOrderNo(order1.getTradingOrderNo());
        billDto.setTransactionStatus(BillStatus.CLOSE.getOrdinal());
        billService.updateBytradingOrderNoSelective(billDto);

        TradingVo tradingVo = new TradingVo();
        tradingVo.setProductOrderNo(order1.getId());
        tradingVo.setTradingOrderNo(order1.getTradingOrderNo());
        tradingVo.setEnterpriseId(1561414331L);
        tradingService.closeTrading(tradingVo);
        return BeanUtil.toBean(order1, OrderVo.class);
    }

    @Override
    public void doOrder(String orderNo) {
        orderMapper.updateStatusBycode(OrderStatus.DONE.getCode(), orderNo);
    }

    @Override
    public OrderVo doOrder(Long orderId) {
        Order order = new Order();
        order.setId(orderId);
        order.setStatus(OrderStatus.DONE.getCode());
        orderMapper.updateByPrimaryKeySelective(order);
        return BeanUtil.toBean(order, OrderVo.class);
    }

    /**
     * 根据订单id获取订单详情
     *
     * @param orderId 订单id
     * @return 订单vo
     */
    @Override
    public OrderVo getOrderById(Long orderId) {
        Order order = orderMapper.selectByPrimaryKey(orderId);
        return BeanUtil.toBean(order, OrderVo.class);
    }

    /**
     * 搜索订单
     *
     * @param status      订单状态
     * @param orderNo     订单编号
     * @param elderlyName 老人姓名
     * @param creator     创建人
     * @param startTime   开始时间
     * @param endTime     结束时间
     * @param page        页码
     * @param pageSize    每页数量
     * @return 订单分页vo
     */
    @Override
    public PageBean<OrderVo> searchOrders(Integer status, String orderNo, String elderlyName, String creator, LocalDateTime startTime, LocalDateTime endTime, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        Long userId = MobThreadLocal.getUserId();
        Page<Order> lists =
                orderMapper.searchOrders(status, orderNo, elderlyName, creator, startTime, endTime, userId);
        return PageBean.of(lists, OrderVo.class);
    }

    @Override
    public List<OrderVo> listByMemberId(Long id) {
        return orderMapper.listByMemberId(id);
    }

    @Override
    public void save(Order order) {
        orderMapper.updateByPrimaryKey(order);
    }

    @Override
    public List<Order> selectByStatus(Integer status) {
        return orderMapper.selectByStatus(status);
    }


    /**
     * 线上支付
     *
     * @param tradingOrderNos 交易号
     */
    @Override
    public void payOrder(List<Long> tradingOrderNos) {
        orderMapper.batchUpdateByTradingOrderNoSelective(tradingOrderNos, OrderStatus.PENDING_DO.getCode());
        //  生成护理任务 创建时间是支付完成时间
        List<Order> orders = orderMapper.selectByTradingOrderNo(tradingOrderNos);
        List<Bill> bills = billMapper.selectBytradingOrderNo(tradingOrderNos);
        Map<Long, String> billMap = bills.stream().collect(Collectors.toMap(Bill::getTradingOrderNo, Bill::getBillNo));
        List<NursingTask> nursingTasks = new ArrayList<>();
        orders.forEach(v -> {
            NursingTask nursingTask = BeanUtil.toBean(v, NursingTask.class);
            nursingTask.setStatus(1);
            nursingTask.setTaskType((byte) 1);
            nursingTask.setRelNo(billMap.get(v.getTradingOrderNo()));
            nursingTask.setEstimatedServerTime(v.getEstimatedArrivalTime());
            nursingTask.setCreateTime(v.getUpdateTime());
            nursingTask.setCreateBy(v.getCreateBy());
            nursingTask.setRemark(v.getRemark());
            nursingTasks.add(nursingTask);
        });
        if (CollUtil.isEmpty(nursingTasks)) {
            return;
        }
        nursingTaskService.batchInsert(nursingTasks);
    }

    @Resource
    private NursingTaskMapper nursingTaskMapper;

    /**
     * 线上退款
     *
     * @param tradingOrderNos 交易号
     */
    @Override
    public void refundOrder(List<Long> tradingOrderNos) {
        orderMapper.batchUpdateByTradingOrderNoSelective(tradingOrderNos, OrderStatus.REFUND.getCode());

        List<Bill> bills = billMapper.selectBytradingOrderNo(tradingOrderNos);
        List<String> collect = bills.stream().map(Bill::getBillNo).collect(Collectors.toList());
        nursingTaskMapper.updateByBillNoSelective(collect);
    }

    @Override
    public void delete(Long orderId) {
        orderMapper.deleteByPrimaryKey(orderId);
    }

    @Override
    public OrderVo createOrderCheck(OrderDto orderDto) {
        ElderVo elderVo = elderService.selectByPrimaryKey(orderDto.getElderId());
        if (elderVo.getStatus() == ElderStatusEnum.RETREAT.getOrdinal()) {
            throw new BaseException(BasicEnum.CANNOT_RESERVATION_DUE_ELDER_ALREADY_RETREATED);
        }
        return null;
    }
}

