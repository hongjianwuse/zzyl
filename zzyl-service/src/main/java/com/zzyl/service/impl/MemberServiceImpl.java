package com.zzyl.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.entity.Member;
import com.zzyl.mapper.MemberMapper;
import com.zzyl.service.ContractService;
import com.zzyl.service.MemberElderService;
import com.zzyl.service.MemberService;
import com.zzyl.service.OrderService;
import com.zzyl.vo.ContractVo;
import com.zzyl.vo.MemberElderVo;
import com.zzyl.vo.MemberVo;
import com.zzyl.vo.OrderVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MemberServiceImpl implements MemberService {

    private static final List<String> DEFAULT_NICKNAME_PREFIX =
            List.of("生活更美好", "大桔大利", "日富一日", "好柿开花", "柿柿如意", "一椰暴富", "大柚所为", "杨梅吐气", "天生荔枝");

    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private ContractService contractService;
    @Autowired
    private MemberElderService memberElderService;
    @Autowired
    private OrderService orderService;

    /**
     * 根据id查询用户
     *
     * @param id 用户id
     * @return 用户信息
     */
    @Override
    public Member getById(Long id) {
        return memberMapper.selectById(id);
    }

    /**
     * 分页查询用户列表
     *
     * @param page     当前页码
     * @param pageSize 每页数量
     * @param phone    手机号
     * @param nickname 昵称
     * @return 分页结果
     */
    @Override
    public PageBean<MemberVo> page(Integer page, Integer pageSize, String phone, String nickname) {
        PageHelper.startPage(page, pageSize);
        Page<Member> listPage = memberMapper.page(phone, nickname);

        PageBean<MemberVo> pageBean = PageBean.of(listPage, MemberVo.class);

        pageBean.getRecords().forEach(memberVo -> {
            List<ContractVo> contractVos = contractService.listByMemberPhone(memberVo.getPhone());
            memberVo.setIsSign(contractVos.isEmpty() ? "否" : "是");
            List<OrderVo> orderVos = orderService.listByMemberId(memberVo.getId());
            memberVo.setOrderCount(orderVos.size());
            List<MemberElderVo> memberElderVos = memberElderService.listByMemberId(memberVo.getId());
            List<String> collect = memberElderVos.stream().map(m -> m.getElderVo().getName()).collect(Collectors.toList());
            memberVo.setElderNames(String.join(",", collect));
        });
        return pageBean;
    }
}
