package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.entity.Member;
import com.zzyl.vo.MemberVo;

public interface MemberService {

    /**
     * 根据id查询用户
     *
     * @param id 用户id
     * @return 用户信息
     */
    Member getById(Long id);

    /**
     * 分页查询用户列表
     *
     * @param page     当前页码
     * @param pageSize 每页数量
     * @param phone    手机号
     * @param nickname 昵称
     * @return 分页结果
     */
    PageBean<MemberVo> page(Integer page, Integer pageSize, String phone, String nickname);
}
