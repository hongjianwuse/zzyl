package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.entity.AlertData;
import com.zzyl.vo.AlertDataVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface AlertDataMapper {

    int deleteByPrimaryKey(Long id);

    int insert(AlertData record);

    int insertSelective(AlertData record);

    AlertData selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(AlertData record);

    int updateByPrimaryKey(AlertData record);

    int batchInsert(@Param("list") List<AlertData> list);

    Page<AlertDataVo> adminPageQuery(@Param("id") Long id,
                                     @Param("status") Integer status,
                                     @Param("deviceName") String deviceName,
                                     @Param("userId") Long userId,
                                     @Param("minCreateTime") LocalDateTime minCreateTime,
                                     @Param("maxCreateTime") LocalDateTime maxCreateTime);

    void handleAlertData(@Param("id") Long id,
                         @Param("processingResult") String processingResult,
                         @Param("processingTime") LocalDateTime processingTime,
                         @Param("processorName") String processorName,
                         @Param("updateBy") Long updateBy,
                         @Param("iotId") String iotId);


    void updateStatus(@Param("iotId") String iotId,
                      @Param("userId") Long userId,
                      @Param("realName") String realName,
                      @Param("processingResult") String processingResult,
                      @Param("processingTime") LocalDateTime processingTime);
}