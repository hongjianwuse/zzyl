package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.dto.DevicePageQueryDto;
import com.zzyl.entity.Device;
import com.zzyl.vo.DeviceVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DeviceMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Device record);

    int insertSelective(Device record);

    Device selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Device record);

    int updateByPrimaryKey(Device record);

    int batchInsert(@Param("list") List<Device> list);

    @Select("select count(1) from device where device_name = #{deviceName}")
    int countByDeviceName(String deviceName);

    Page<DeviceVo> selectPage(DevicePageQueryDto devicePageQueryDto);

    DeviceVo selectByIotId(String iotId);

    @Delete(" delete from device where iot_id = #{iotId}")
    void deleteByIotId(String iotId);

    List<Long> selectNursingIdsByIotIdWithElder(String iotId);

    List<Long> selectNursingIdsByIotIdWithBed(String iotId);
}