package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.entity.Contract;
import com.zzyl.vo.ContractVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface ContractMapper {
    /**
     * 根据id删除合同
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 插入合同
     *
     * @param contract 合同信息
     * @return 新增数量
     */
    int insert(Contract contract);

    /**
     * 选择性插入合同
     */
    int insertSelective(Contract record);

    /**
     * 根据id选择合同
     */
    Contract selectByPrimaryKey(Long id);

    /**
     * 选择性更新合同
     */
    int updateByPrimaryKeySelective(Contract record);

    /**
     * 选择性更新合同
     */
    int batchUpdateByPrimaryKeySelective(@Param("ids") List<Long> ids,
                                         @Param("status") Integer status);

    /**
     * 更新合同
     */
    int updateByPrimaryKey(Contract record);


    /**
     * 分页查询合同信息
     *
     * @param contractNo 合同编号
     * @param elderName  老人姓名
     * @param status     合同状态
     * @param startTime  开始时间
     * @param endTime    结束时间
     * @return 分页结果
     */
    Page<Contract> selectByPage(@Param("memberPhone") String memberPhone,
                                      @Param("contractNo") String contractNo,
                                      @Param("elderName") String elderName,
                                      @Param("status") Integer status,
                                      @Param("startTime") LocalDateTime startTime,
                                      @Param("endTime") LocalDateTime endTime);


    List<Contract> listAllContracts();

    List<ContractVo> listByMemberPhone(@Param("memberPhone") String memberPhone);

    /**
     * 根据合同编号查询合同
     *
     * @param contractNo 合同编号
     * @return 合同信息
     */
    @Select("select * from contract where contract_no = #{contractNo}")
    Contract selectByContractNo(@Param("contractNo") String contractNo);

    /**
     * 根据老人id查询最新合同
     *
     * @param elderId 老人id
     * @return 合同
     */
    @Select("select * from contract where elder_id = #{elderId} order by id desc limit 1")
    Contract selectLastByElderId(@Param("elderId") Long elderId);

    /**
     * 根据合同状态查询合同
     *
     * @param status 合同状态{@link com.zzyl.enums.ContractStatusEnum}
     * @return 合同列表
     */
    @Select("select * from contract where status = #{status}")
    List<Contract> listByStatus(@Param("status") Integer status);
}

