package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.entity.Elder;
import com.zzyl.vo.ElderCheckInInfoVo;
import com.zzyl.vo.ElderPageQueryVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ElderMapper {

    /**
     * 根据主键删除老人信息
     *
     * @param id 主键
     * @return 删除结果
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 插入老人信息
     *
     * @param elder 老人信息
     * @return 插入结果
     */
    int insert(Elder elder);

    /**
     * 选择性插入老人信息
     *
     * @param elder 老人信息
     * @return 插入结果
     */
    int insertSelective(Elder elder);

    /**
     * 根据主键选择老人信息
     *
     * @param id 主键
     * @return 老人信息
     */
    Elder selectByPrimaryKey(Long id);

    /**
     * 根据主键选择性更新老人信息
     *
     * @param elder 老人信息
     * @return 更新结果
     */
    int updateByPrimaryKeySelective(Elder elder);

    /**
     * 根据主键更新老人信息
     *
     * @param elder 老人信息
     * @return 更新结果
     */
    int updateByPrimaryKey(Elder elder);


    /**
     * 根据身份证号和姓名选择老人信息
     *
     * @param idCard 身份证号
     * @param name   姓名
     * @return 老人信息
     */
    Elder selectByIdCardAndName(@Param("idCard") String idCard,
                                @Param("name") String name);

    List<Elder> selectList();

    List<Elder> selectByIds(List<Long> ids);

    /**
     * 根据身份证号和状态查询老人
     *
     * @param idCard 身份证号
     * @param status 状态{@link com.zzyl.enums.ElderStatusEnum}
     * @return 老人信息
     */
    Elder selectByIdCardAndStatus(@Param("idCard") String idCard,
                                  @Param("status") Integer status);

    @Update("UPDATE elder SET bed_number = null ,bed_id = null WHERE id = #{elderId}")
    void clearBedNum(Long elderId);

    /**
     * 老人信息分页查询
     *
     * @param name     老人姓名，模糊查询
     * @param idCardNo 身份证号，精确查询
     * @param status   状态，0：入住中，1：已退住
     * @return 分页结果
     */
    Page<ElderPageQueryVo> pageQuery(@Param("name") String name,
                                     @Param("idCardNo") String idCardNo,
                                     @Param("status") String status);

    /**
     * 老人入住信息查询
     *
     * @param id 老人id
     * @return 老人入住信息
     */
    ElderCheckInInfoVo checkInInfo(@Param("id") Long id);
}

