package com.zzyl.mapper;

import com.zzyl.entity.Bed;
import com.zzyl.vo.BedVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface BedMapper {

    /**
     * 根据id获取床位信息
     *
     * @param id 床位id
     * @return 床位对象
     */
    Bed getBedById(Long id);


    /**
     * 根据房间id获取床位信息列表
     *
     * @param roomId 房间id
     * @return 床位对象列表
     */
    List<BedVo> getBedsByRoomId(@Param("roomId") Long roomId);

    /**
     * 根据id更新床位状态
     *
     * @param id        床位id
     * @param bedStatus 床位状态
     */
    @Update("update bed set bed_status = #{bedStatus} where id = #{id}")
    void updateBedStatusById(@Param("id") Long id,
                             @Param("bedStatus") Integer bedStatus);
}

