package com.zzyl.dto;

import lombok.Data;

@Data
public class LoginDto {

    private String username; //用户名
    private String password; //密码
}
