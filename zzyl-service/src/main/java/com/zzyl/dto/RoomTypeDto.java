package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(description = "房型信息")
public class RoomTypeDto extends BaseDto {

    private String name; // 房型名称
    private Integer bedCount; // 床位数量
    private BigDecimal price; // 床位费用
    private String introduction; // 介绍
    private String photo; // 照片
    private String typeName; // 类型名称
    private Integer status; // 状态，0：禁用，1：启用
}
