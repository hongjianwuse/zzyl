package com.zzyl.dto;

import com.aliyun.iot20180120.models.RegisterDeviceRequest;
import com.aliyun.tea.NameInMap;
import com.zzyl.base.BaseDto;
import lombok.Data;

@Data
public class DeviceDto extends BaseDto {

    @NameInMap("IotId")
    private String iotId;
    @NameInMap("Nickname")
    private String nickname;
    @NameInMap("ProductKey")
    private String productKey;

    private RegisterDeviceRequest registerDeviceRequest; // 注册参数
    private String productName; // 产品名称
    private String deviceDescription; // 位置名称回显字段
    private Integer locationType; // 位置类型 0 老人 1位置
    private Long bindingLocation; // 绑定位置
    private String deviceName; // 设备名称
    private Integer physicalLocationType; // 物理位置类型 0楼层 1房间 2床位
}