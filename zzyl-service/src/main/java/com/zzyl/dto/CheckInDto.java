package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel("入住实体类")
public class CheckInDto extends BaseDto {

    private Integer save; // 保存1 提交2
    private ElderDto elderDto; // 老人
    private String otherApplyInfo; // 其他申请信息
    private String reviewInfo; // 健康评估信息
    private String reviewInfo1; // 能力评估
    private String reviewInfo2; // 评估报告
    private LocalDateTime checkInTime; // 入住时间
    private List<MemberElderDto> memberElderDtos; // 家属信息
    private String url1; // 一寸照片
    private String url2; // 身份证人像面
    private String url3; // 身份证国徽面
    private String taskId; // 任务ID


}
