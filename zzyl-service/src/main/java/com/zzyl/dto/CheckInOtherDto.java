package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(description = "入住实体类")
public class CheckInOtherDto {

    private List<MemberElderDto> memberElderDtos; // 家属信息
    private String url1; // 一寸照片
    private String url2; // 身份证人像面
    private String url3; // 身份证国徽面
}
