package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "RoomDto", description = "客房信息")
public class RoomDto extends BaseDto {

    private String code; // 客房编号
    private Integer sort; // 客房排序
    private String typeName; // 客房类型名称
    private Long floorId; // 楼层id
}
