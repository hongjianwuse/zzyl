package com.zzyl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel("余额信息")
public class BalanceDto extends BaseDto {

    private BigDecimal prepaidBalance; // 预付款余额
    private BigDecimal depositAmount; // 押金金额
    private BigDecimal arrearsAmount; // 欠费金额
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime paymentDeadline; // 缴费截止日期
    private Integer status; // 状态
    private Long elderId; // 老人ID
    private String elderName; // 老人姓名
    private String bedNo; // 床位号
}
