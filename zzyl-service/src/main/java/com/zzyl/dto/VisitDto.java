package com.zzyl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseDto;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class VisitDto extends BaseDto {

    private String name; // 来访人
    private String mobile; // 来访人手机号
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time; // 时间
    private String visitor; // 探访人
    private Integer type; // 来访类型，0：参观来访，1：探访来访
    private Integer status; // 来访状态，0：待报道，1：已完成，2：取消，3：过期

}
