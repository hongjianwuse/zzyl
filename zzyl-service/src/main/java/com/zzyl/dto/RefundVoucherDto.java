package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("退款凭证上传请求模型")
public class RefundVoucherDto {

    private String refundVoucherUrl; // 退款凭证URL
    private String tradingChannel; // 退款渠道【支付宝、微信、现金】
    private Long elderId; // 老人id
    private BigDecimal refundAmount; // 退款金额
    private String remark; // 备注
}
