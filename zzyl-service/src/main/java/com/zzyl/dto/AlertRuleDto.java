package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import lombok.Data;

@Data
public class AlertRuleDto extends BaseDto {

    private String productKey; // 产品ID
    private String productName; // 产品名称
    private String moduleId; // 模块ID
    private String moduleName; // 模块名称
    private String functionName; // 功能名称
    private String functionId; // 功能ID
    private String iotId; // 物联网设备ID
    private String deviceName; // 设备名称
    private Integer alertDataType; // 报警数据类型，0：老人异常数据，1：设备异常数据
    private String alertRuleName; // 规则名称
    private String operator; // 操作符
    private Float value; // 值
    private Integer duration; // 持续时间
    private String alertEffectivePeriod; // 告警生效周期
    private Integer alertSilentPeriod; // 告警静默期
    private Integer status; // 状态 0-无效 1-有效
}