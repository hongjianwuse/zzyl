package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("报警数据分页查询请求模型")
public class AlertDataPageQueryDto {

    private Long startTime; // 开始报警时间
    private Long endTime; // 结束报警时间
    private String deviceName; // 设备名称（精确搜索）
    private Integer status; // 状态，0：待处理，1：已处理
    private Long id; // 报警数据id
    private Integer pageNum; // 页码
    private Integer pageSize; // 页面大小
}