package com.zzyl.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 床位状态枚举
 *
 * @author itheima
 */
@Getter
@AllArgsConstructor
public enum BedStatusEnum {
    UNOCCUPIED(0, "未入住"),
    OCCUPIED(1, "已入住");

    /**
     * 序号值
     */
    private final Integer ordinal;

    /**
     * 描述
     */
    private final String name;
}

