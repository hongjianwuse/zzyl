package com.zzyl.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author itcast
 */
@Getter
@AllArgsConstructor
public enum DeviceLocationTypeEnum {

    PORTABLE_DEVICE(0, "随身设备"),

    FIXED_DEVICE(1, "固定设备");

    /**
     * 序号值
     */
    private final Integer ordinal;

    /**
     * 描述
     */
    private final String name;
}