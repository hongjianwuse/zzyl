package com.zzyl.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 护理任务状态
 *
 * @author itcast
 */
@Getter
@AllArgsConstructor
public enum NursingTaskStatusEnum {

    WAITING(1, "待执行"),

    COMPLETED(2, "已执行"),

    CLOSED(3, "已关闭");

    /**
     * 序号值
     */
    private final int ordinal;

    /**
     * 描述
     */
    private final String name;
}
