package com.zzyl.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 合同状态枚举
 *
 * @author itcast
 */
@Getter
@AllArgsConstructor
public enum ContractStatusEnum {

    PENDING_EFFECTIVE(0, "未生效"),

    EFFECTIVE(1, "生效中"),

    EXPIRED(2, "已过期"),

    UN_EFFECTIVE(3, "已失效");

    /**
     * 序号值
     */
    private final Integer ordinal;

    /**
     * 描述
     */
    private final String name;
}
