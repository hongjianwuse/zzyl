package com.zzyl.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author itcast
 */
@Getter
@AllArgsConstructor
public enum DevicePhysicalLocationTypeEnum {

    ELDER(-1, "老人"),

    FLOOR(0, "楼层"),

    ROOM(1, "房间"),

    BED(2, "床位");

    /**
     * 序号值
     */
    private final Integer ordinal;

    /**
     * 描述
     */
    private final String name;
}