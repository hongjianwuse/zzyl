package com.zzyl.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 余额状态枚举
 *
 * @author itheima
 */
@Getter
@AllArgsConstructor
public enum BalanceStatusEnum {
    /**
     * 正常
     */
    NORMAL(0, "正常"),

    /**
     * 退住
     */
    RETREAT(1, "退住");

    /**
     * 序号值
     */
    private final Integer ordinal;

    /**
     * 描述
     */
    private final String name;
}

