package com.zzyl.vo;


import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 老人实体类
 */
@Data
@ApiModel(description = "选择老人实体类")
public class ChoiceElderVo {

    private Long elderId; // 老人id
    private String name; // 姓名
    private String idCardNo; // 身份证号
    private String phone; // 联系方式
    private String checkInStartTime; // 入住开始时间
    private String checkInEndTime; // 入住结束时间
    private String nursingLevelName; // 护理等级
    private String bedNo; // 入住床位
    private String contractName; // 签约合同
    private String contractUrl; // 合同URL
    private String contractNo; // 合同编号
    private String counselor; // 养老顾问
    private String nursingName; // 护理员名称
}
