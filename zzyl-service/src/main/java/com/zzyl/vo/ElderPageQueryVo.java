package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "老人分页查询响应模型")
public class ElderPageQueryVo {

    private Long id; // 老人id
    private String name; // 老人姓名
    private String idCardNo; // 老人身份证号
    private String bedNumber; // 床位号
}
