package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "登录对象")
public class LoginVo {

    private String token; // JWT token
    private String nickName; // 昵称
}
