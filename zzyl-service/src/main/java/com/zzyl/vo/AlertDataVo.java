package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("报警数据响应模型")
public class AlertDataVo {

    private Long id; // 报警数据id
    private String iotId; // 物联网设备id
    private String deviceName; // 设备名称
    private String nickname; // 设备备注名称
    private String productKey; // 所属产品key
    private String productName; // 产品名称
    private String functionId; // 功能标识符
    private String accessLocation; // 接入位置
    private String dataValue; // 数据值
    private String alertRuleId; // 报警规则id
    private String alertReason; // 报警原因，格式:功能名称+运算符号+阙值,持续x个周期就报警. 示例:心率>=100,持续3个周期就报警
    private String processingResult; // 处理结果
    private String processorId; // 处理人id
    private String processorName; // 处理人名称
    private String processingTime; // 处理时间
    private String type; // 报警数据类型，0：老人异常数据，1：设备异常数据
    private String status; // 状态，0：待处理，1：已处理
    private Long userId; // 用户id
    private LocalDateTime createTime; // 创建时间
}