package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel(value = "ContractVo", description = "合同信息")
public class ContractVo extends BaseVo {
    private String name; // 合同名称
    private String memberPhone; // 丙方手机号
    private String memberName; // 丙方名称
    private String elderName; // 老人名称
    private String contractNo; // 合同编号
    private String pdfUrl; // 合同pdf文件地址
    private Long memberId; // 会员id
    private Long elderId; // 老人id
    private LocalDateTime startTime; // 合同开始时间
    private LocalDateTime endTime; // 合同结束时间
    private Integer status; // 合同状态
    private Integer sort; // 排序
    private String levelDesc; // 级别描述
    private String checkInNo; // 入住编号
    private LocalDateTime signDate; // 签约时间
    private String releaseSubmitter; // 解除提交人
    private LocalDateTime releaseDate; // 解除时间
    private String releasePdfUrl; // 解除pdf文件地址
    private ElderVo elderVo; // 老人信息
    private RoomVo roomVo; // 房间信息
}


