package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class NursingTaskVo extends BaseVo {

    private Long id; // id
    private Long nursingId; // 护理员id
    private Long elderId; // 老人id
    private Integer projectId; // 项目id
    private String bedNumber; // 床位编号
    private Byte taskType; // 任务类型（0：月度任务，1订单任务）
    private LocalDateTime estimatedServerTime; // 预计服务时间
    private LocalDateTime realServerTime; // 实际服务时间
    private String mark; // 执行记录
    private String cancelReason; // 取消原因
    private Integer status; // 状态  1待执行 2已执行 3已关闭
    private String relNo; // 关联单据编号
    private String taskImage; // 执行图片
    private String projectName; // 护理项目名称
    private String elderName; // 老人名称
    private List<String> nursingName; // 护理员名称
    private String lName; // 护理等级名称
    private String idCardNo; // 身份证号
    private String image; // 头像
    private String sex; // 性别
}