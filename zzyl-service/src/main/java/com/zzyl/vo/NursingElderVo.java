package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import lombok.Data;

@Data
public class NursingElderVo extends BaseVo {

    private Long id; // id
    private Long nursingId; // 护理员id
    private Long elderId; // 老人id
}