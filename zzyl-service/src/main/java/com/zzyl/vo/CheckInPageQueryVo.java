package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel(description = "入住信息分页结果响应模型")
public class CheckInPageQueryVo {

    private Long id; // 入住id
    private String elderName; // 老人姓名
    private String elderIdCardNo; // 老人身份证号
    private String bedNumber; // 床位号
    private String nursingLevelName; // 护理等级名称
    private LocalDateTime checkInStartTime; // 入住开始时间
    private LocalDateTime checkInEndTime; // 入住结束时间
    private String applicat; // 申请人
    private LocalDateTime createTime; // 创建时间
}
