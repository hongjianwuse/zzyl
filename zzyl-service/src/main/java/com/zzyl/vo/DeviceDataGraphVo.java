package com.zzyl.vo;

import cn.hutool.core.date.LocalDateTimeUtil;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 设备数据曲线图响应模型
 **/
@Data
@ApiModel("设备数据曲线图响应模型")
public class DeviceDataGraphVo {

    private String dateTime; // 日期
    private Double dataValue; // 数据

    /**
     * 构建按日统计数据示例
     */
    public static List<DeviceDataGraphVo> dayInstance(LocalDateTime startTime) {
        List<DeviceDataGraphVo> list = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            LocalDateTime dataTime = startTime.plusHours(i);
            DeviceDataGraphVo deviceDataGraphVo = new DeviceDataGraphVo();
            deviceDataGraphVo.setDateTime(LocalDateTimeUtil.format(dataTime, "HH:00"));
            deviceDataGraphVo.setDataValue(0.0);
            list.add(deviceDataGraphVo);
        }
        return list;
    }

    /**
     * 构建按周统计数据示例
     */
    public static List<DeviceDataGraphVo> weekInstance(LocalDateTime startTime) {
        List<DeviceDataGraphVo> list = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            LocalDateTime dataTime = startTime.plusDays(i);
            DeviceDataGraphVo deviceDataGraphVo = new DeviceDataGraphVo();
            deviceDataGraphVo.setDateTime(LocalDateTimeUtil.format(dataTime, "MM.dd"));
            deviceDataGraphVo.setDataValue(0.0);
            list.add(deviceDataGraphVo);
        }
        return list;
    }
}
