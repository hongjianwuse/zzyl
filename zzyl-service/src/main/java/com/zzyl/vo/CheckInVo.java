package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import com.zzyl.dto.ElderDto;
import com.zzyl.dto.MemberElderDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel(description = "入住实体类")
public class CheckInVo extends BaseVo {

    private String checkInCode; // 入住单号
    private String title; // 入住标题
    private ElderDto elderDto; // 老人
    private Long elderId; // 老人id
    private String otherApplyInfo; // 其他申请信息
    private String reviewInfo; // 评估信息
    private String counselor; // 养老顾问
    private LocalDateTime checkInTime; // 入住时间
    private String reason; // 入住原因
    private String remark; // 备注
    private String applicat; // 申请人
    private String deptNo; // 申请人部门编号
    private Long applicatId; // 申请人id
    private LocalDateTime createTime; // 申请时间
    private Integer status; // 状态（1：申请中，2:已完成,3:已关闭）
    private Integer isShow; // 是否展示退住数据
    private List<MemberElderDto> memberElderDtos; // 家属信息
    private String url1; // 一寸照片
    private String url2; // 身份证人像面
    private String url3; // 身份证国徽面
    private String reviewInfo1; // 能力评估
    private String reviewInfo2; // 评估报告
    private RoomVo roomVo; // 房间
    private CheckInConfigVo checkInConfigVo; // 入住配置
    private NursingLevelVo nursingLevelVo; // 护理等级
    private BedVo bedVo; // 床位
    private ContractVo contractVo; // 签约信息
}
