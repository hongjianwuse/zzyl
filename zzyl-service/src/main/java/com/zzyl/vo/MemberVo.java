package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MemberVO", description = "用户信息DTO")
public class MemberVo extends BaseVo {

    private String authId; // 认证id
    private String idCardNo; // 身份证号
    private Integer idCardNoVerify; // 身份证号是否认证 1认证
    private String phone; // 手机号
    private String name; // 姓名
    private String avatar; // 头像
    private String openId; // openId
    private Integer sex; // 性别
    private String birthday; // 生日
    private Integer orderCount; // 下单次数
    private String isSign; // 是否签约
    private String elderNames; // 老人姓名
}


