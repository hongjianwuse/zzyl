package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("退住分页查询响应模型")
public class RetreatPageQueryVo {

    private Long id; // 退住id
    private String elderName; // 老人姓名
    private String elderIdCardNo; // 老人身份证号
    private LocalDateTime checkOutTime; // 退住时间
    private String applicat; // 创建人
    private LocalDateTime createTime; // 创建时间
}
