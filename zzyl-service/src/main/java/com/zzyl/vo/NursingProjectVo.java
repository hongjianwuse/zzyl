package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 护理项目视图对象
 */
@Data
public class NursingProjectVo extends BaseVo {

    private String name; // 名称
    private Integer orderNo; // 排序号
    private String unit; // 单位
    private BigDecimal price; // 价格
    private String image; // 图片
    private String nursingRequirement; // 护理要求
    private Integer status; // 状态（0：禁用，1：启用）
    private Integer count; // 护理项目绑到计划的个数
}
