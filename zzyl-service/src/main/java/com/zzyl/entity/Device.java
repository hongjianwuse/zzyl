package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Device extends BaseEntity {

    private String iotId; // 物联网设备ID
    private String bindingLocation; // 绑定位置
    private Integer locationType; // 位置类型：0随身设备，1固定设备
    private Integer physicalLocationType; // 物理位置类型 0楼层 1房间 2床位   -1随身设备
    private String deviceName; // 设备名称
    private String deviceDescription; // 位置备注
    private Integer haveEntranceGuard; // 产品是否包含门禁，0：否，1：是
    private String nickname; // 备注名称
    private String productKey; // 产品key
    private String productName; // 产品名称
}