package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

/**
 * 充值记录实体类
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PrepaidRechargeRecord extends BaseEntity {

    private Long id; // 主键id
    private BigDecimal rechargeAmount; // 充值金额
    private String rechargeVoucher; // 充值凭证
    private String rechargeMethod; // 充值方式
    private Long elderId; // 老人id
    private String elderName; // 老人姓名
    private String idCardNo; // 床位号
    private String prepaidRechargeNo; // 编号
}
