package com.zzyl.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "退住账单实体类")
public class RetreatBill implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id; // 主键
    private String billJson; // 账单json数据
    private Long createBy; // 创建人
    private LocalDateTime createTime; // 创建时间
    private Long elderId; // 老人ID
    private Integer isRefund; // 是否退款
    private BigDecimal refundAmount; // 退款金额
    private String refundVoucherUrl; // 退款凭证URL
    private String remark; // 备注
    private Long retreatId; // 退住id
    private String tradingChannel; // 支付渠道
    private Long updateBy; // 更新人
    private LocalDateTime updateTime; // 更新时间
}
