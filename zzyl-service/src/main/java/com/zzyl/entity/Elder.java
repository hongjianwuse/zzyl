package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 老人实体类
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Elder extends BaseEntity {

    private String name; // 姓名
    private String image; // 头像
    private Integer status; // 状态 ElderStatusEnum
    private String idCardNo; // 身份证号
    private String phone; // 手机号
    private String sex; // 性别，0：男，1：女，2：未知
    private String birthday; // 出生日期，格式：yyyy-MM-dd
    private String address; // 家庭住址
    private String idCardNationalEmblemImg; // 身份证国徽面
    private String idCardPortraitImg; // 身份证人像面
    private String bedNumber; // 床位编号
    private Long bedId; // 床位ID
}

