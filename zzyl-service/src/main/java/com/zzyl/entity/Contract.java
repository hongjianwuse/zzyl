package com.zzyl.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseEntity;
import com.zzyl.vo.RoomVo;
import com.zzyl.vo.retreat.ElderVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

/**
 * 合同实体类
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Contract extends BaseEntity {

    private String name; // 合同名称
    private String memberPhone; // 丙方手机号
    private String memberName; // 丙方名称
    private String elderName; // 老人名称
    private String contractNo; // 合同编号
    private String pdfUrl; // 合同pdf文件地址
    private Long memberId; // 会员id
    private Long elderId; // 老人id
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime; // 合同开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime; // 合同结束时间
    private Integer status; // 合同状态
    private Integer sort; // 排序
    private String levelDesc; // 级别描述
    private String checkInNo; // 入住编号
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime signDate; // 签约时间
    private String releaseSubmitter; // 解除提交人
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime releaseDate; // 解除时间
    private String releasePdfUrl; // 解除pdf文件地址
    private ElderVo elderVo;
    private RoomVo roomVo;
}

