package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "NursingLevel对象", description = "护理等级实体类")
public class NursingLevel extends BaseEntity {

    private String name; // 等级名称
    private String planName; // 护理计划名称
    private Long planId; // 护理计划ID
    private BigDecimal fee; // 护理费用
    private Integer status; // 状态（0：禁用，1：启用）
    private String description; // 等级说明
}
