package com.zzyl.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 入住配置实体类
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CheckInConfig extends BaseEntity {

    private Long id; // 主键id
    private Long elderId; // 老人id
    private String checkInCode; // 入住编码
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkInStartTime; // 入住开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkInEndTime; // 入住结束时间
    private Long nursingLevelId; // 护理等级id
    private String nursingLevelName; // 护理等级名称
    private String bedNumber; // 床位号
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime costStartTime; // 费用开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime costEndTime; // 费用结束时间
    private BigDecimal depositAmount; // 押金金额
    private BigDecimal nursingCost; // 护理费用
    private BigDecimal bedCost; // 床位费用
    private BigDecimal otherCost; // 其他费用
    private BigDecimal medicalInsurancePayment; // 医保支付金额
    private BigDecimal governmentSubsidy; // 政府补贴金额
    private NursingLevel nursingLevel; // 护理等级
}
