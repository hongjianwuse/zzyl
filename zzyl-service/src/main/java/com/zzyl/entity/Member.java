package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 实体类
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Member extends BaseEntity {

    private String phone; // 手机号
    private String name; // 名称
    private String avatar; // 头像
    private String openId; // OpenID
    private Integer gender; // 性别(0:男，1:女)
}
