package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Bed extends BaseEntity {

    private String bedNumber; // 床位编号
    private Integer bedStatus; // 床位状态
    private Long roomId; // 房间ID
    private Integer sort; // 排序号
}
