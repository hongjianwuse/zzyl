package com.zzyl.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * 余额实体类
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Balance extends BaseEntity {

    private BigDecimal prepaidBalance; // 预付款余额
    private BigDecimal depositAmount; // 押金金额
    private BigDecimal arrearsAmount; // 欠费金额
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime paymentDeadline; // 缴费截止日期
    private Integer status; // 状态（0：正常，1：退住）
    private Long elderId; // 老人id
    private String elderName; // 老人姓名
    private String bedNo; // 床位号
}
