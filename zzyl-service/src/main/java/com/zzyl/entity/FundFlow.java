package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;


/**
 * 资金流水实体类
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class FundFlow extends BaseEntity {

    private Long id; // 主键id
    private Integer balanceType; // 余额类型
    private Integer fundDirection; // 资金流向
    private String relatedBillNo; // 相关单据号
    private String flowReason; // 流水原因
    private BigDecimal amount; // 金额
    private Long elderId; // 老人id
    private String elderName; // 老人姓名
    private String bedNo; // 床位号
}

