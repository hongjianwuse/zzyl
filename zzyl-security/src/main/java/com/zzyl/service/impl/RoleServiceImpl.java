package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.zzyl.entity.Role;
import com.zzyl.mapper.RoleMapper;
import com.zzyl.service.RoleService;
import com.zzyl.vo.RoleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    /**
     * 角色下拉框
     */
    @Override
    public List<RoleVo> list() {
        List<Role> roleList = roleMapper.list();
        return BeanUtil.copyToList(roleList, RoleVo.class);
    }
}
