package com.zzyl.service;

import com.zzyl.vo.RoleVo;

import java.util.List;

public interface RoleService {

    /**
     * 角色下拉框
     */
    List<RoleVo> list();
}
