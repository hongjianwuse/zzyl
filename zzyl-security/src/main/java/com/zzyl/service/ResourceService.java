package com.zzyl.service;

import com.zzyl.vo.MenuVo;

import java.util.List;

public interface ResourceService {

    /**
     * 根据用户id查询对应的资源数据
     *
     * @param userId
     * @return
     */
    List<MenuVo> menus(Long userId);
}
