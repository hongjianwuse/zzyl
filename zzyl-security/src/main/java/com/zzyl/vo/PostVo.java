package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel("岗位VO")
public class PostVo extends BaseVo {

    private String deptNo; // 部门编号
    private String postNo; // 岗位编码：父部门编号+001【3位】
    private String postName; // 岗位名称
    private Integer sortNo; // 显示顺序
    private String remark; // 备注
    private String[] checkedIds; // 列表选择：选中职位Ids
    private DeptVo deptVo; // 职位对应部门
}
