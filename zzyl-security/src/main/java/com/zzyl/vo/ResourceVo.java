package com.zzyl.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel("资源菜单VO")
public class ResourceVo extends BaseVo {

    private String resourceNo; // 资源编号
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private String parentResourceNo; // 父资源编号
    private String resourceName; // 资源名称
    private String resourceType; // 资源类型：s平台 c目录 m菜单 r按钮
    private String requestPath; // 请求地址
    private String label; // 权限标识
    private Integer sortNo; // 排序
    private String icon; // 图标
    private String remark; // 备注
    private String[] checkedIds; // 列表选择：选中职位Ids
    private String[] checkedResourceNos; // TREE结构：选中资源编号
    private String roleId; // 角色查询资源：资源对应角色id
    private Integer level = 4; // 层级
}
