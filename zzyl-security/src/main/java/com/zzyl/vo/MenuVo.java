package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel("菜单VO")
public class MenuVo implements Serializable {

    // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
    private String name; // 路由名字
    private String resourceNo; // 资源编号
    private String parentResourceNo; // 父资源编号
    private String path; // 请求路径
    private String redirect; // 层级名称展示
    private List<MenuVo> children = new ArrayList<>(); // 子菜单
    private MenuMetaVo meta; // meta属性
}