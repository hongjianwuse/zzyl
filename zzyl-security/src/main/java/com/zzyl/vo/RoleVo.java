package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel("角色VO")
public class RoleVo extends BaseVo {

    private String roleName; // 角色名称
    private String label; // 角色标识
    private Integer sortNo; // 排序
    private String remark; // 备注
    private String[] checkedIds; // 列表选择：选中角色Ids
    private String[] checkedResourceNos; // TREE结构：选中资源No
    private String[] checkedDeptNos; // TREE结构：选中部门No
    private String userId; // 人员查询部门：当前人员Id
    private String dataScope; // 数据范围（0自定义  1本人 2本部门及以下 3本部门 4全部）
    private String dataState; // 数据状态
}
