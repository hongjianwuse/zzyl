package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.dto.PostDto;
import com.zzyl.entity.Post;
import com.zzyl.vo.PostVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface PostMapper {

    int insert(Post record);

    int insertSelective(Post record);

    @Select("select * from sys_post where id = #{id}")
    Post selectById(Long id);

    Post selectByPostNo(@Param("postNo") String postNo);

    int updateByPrimaryKeySelective(Post record);

    int updateByPrimaryKey(Post record);

    Page<Post> selectPage(@Param("postDto") PostDto postDto);

    List<Post> selectList(@Param("postDto") PostDto postDto);

    List<PostVo> findPostVoListByUserId(@Param("userId") Long userId);

    void deleteByPostNo(@Param("postNo") String postNo);

    void deleteByDeptNo(@Param("deptNo") String deptNo);
}
