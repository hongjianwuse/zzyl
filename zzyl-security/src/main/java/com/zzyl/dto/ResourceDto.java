package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@ApiModel("资源菜单DTO")
public class ResourceDto extends BaseDto {

    private String resourceNo; // 资源编号
    private String parentResourceNo; // 父资源编号
    private String resourceName; // 资源名称
    private String resourceType; // 资源类型：s平台 c目录 m菜单 r按钮
    private String requestPath; // 请求地址
    private String label; // 权限标识
    private Integer sortNo; // 排序
    private String icon; // 图标
    private String remark; // 备注
    private String roleId; // 角色查询资源：资源对应角色id
    private Integer level = 4; // 层级
    private String dataState; // 是否启用（0:启用，1:禁用）
}
