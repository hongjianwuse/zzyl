package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel("修改密码DTO")
public class PasswordDto extends BaseDto {

    private String pw; // 新密码
    private String oldPw; // 旧密码
}