package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import com.zzyl.vo.DeptVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@ApiModel("岗位DTO")
public class PostDto extends BaseDto {
    private String deptNo; // 部门编号
    private String postNo; // 岗位编码：父部门编号+001【3位】
    private String postName; // 岗位名称
    private Integer sortNo; // 显示顺序
    private String remark; // 备注
    private DeptVo deptVo; // 职位对应部门
    private String dataState; // 是否启用(0:启用,1:禁用)
}
