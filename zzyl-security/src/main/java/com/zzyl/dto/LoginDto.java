package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("后台登录DTO")
public class LoginDto {

    private String username; // 用户名
    private String password; // 密码
}
