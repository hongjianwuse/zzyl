package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@Data
@SuperBuilder
@NoArgsConstructor
@ApiModel("用户DTO")
public class UserDto extends BaseDto {

    private String username; // 用户账号
    private String avatar; // 头像地址
    private String dataState; // 是否启用（0:启用，1:禁用）
    private String email; // 用户邮箱
    private String nickName; // 用户昵称
    private String post; // 用户职位
    private String dept; // 用户部门
    private String realName; // 真实姓名
    private String mobile; // 手机号码
    private String sex; // 用户性别（0男 1女 2未知）
    private String remark; // 备注
    private String[] checkedIds; // 选中节点
    private Set<String> roleVoIds; // 查询用户：用户角色Ids
    private String deptNo; // 部门编号【当前】
    private String postNo; // 职位编号【当前】
    private Long roleId; // 角色Id【当前】
}
