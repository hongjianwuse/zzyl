package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class User extends BaseEntity {

    private String avatar;//头像地址
    private String dataState;//数据状态（0正常 1停用）
    private String deptNo;//部门编号
    private String email;//用户邮箱
    private Integer isDelete;//是否删除(0:否，1：是)
    private Integer isLeader;//是否是部门leader(0:否，1：是)
    private String mobile;//手机号码
    private String nickName;//用户昵称
    private String openId;//open_id标识
    private String password;//密码
    private String postNo;//岗位编号
    private String realName;//真实姓名
    private String sex;//用户性别（0男 1女 2未知）
    private String userType;//用户类型（0:系统用户,1:客户）
    private String username;//用户账号
    private String deptName;//部门名称
    private String postName;//岗位名称
}
