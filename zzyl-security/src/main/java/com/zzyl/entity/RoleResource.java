package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RoleResource extends BaseEntity {

    private String dataState;//数据状态（0正常 1停用）
    private String resourceNo;//资源编号
    private Long roleId;//角色ID
}
