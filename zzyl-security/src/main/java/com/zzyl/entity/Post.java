package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Post extends BaseEntity {

    private String deptNo;//部门编号
    private String postNo;//岗位编码：父部门编号+01【2位】
    private String postName;//岗位名称
    private Integer sortNo;//显示顺序
    private String dataState;//数据状态（0正常 1停用）
}
